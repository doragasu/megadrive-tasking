#ifndef _UTIL_H_
#define _UTIL_H_

/// Section attribute definition for variables and functions. Examples:
/// - int a SECTION(data);
/// - void foo(void) SECTION(text);
#define SECTION(name)	__attribute__((section(#name)))

/// Put next symbol in named section .text.ro_text.symbol
#define ROM_TEXT(name)	SECTION(.text.ro_text.name)
/// Put next symbol in named section .text.ro_data.symbol
#define ROM_DATA(name)	SECTION(.text.ro_data.name)

/// Remove compiler warnings when not using a function parameter
#define UNUSED_PARAM(x)		(void)(x)

#endif /*_UTIL_H_*/
