# Megadrive tasking experiment

Based on [this tutorial](https://www.plutiedev.com/multitasking) by @plutiedev.

The code creates 3 tasks:

* A background task that continuously increments a counter.
* A foreground task that once per frame prints the counter value.
* An interrupt routine that implements a frame counter.

A basic locking mechanism is also demonstrated. Depending on the counter value, the foreground task suspends itself, and is later resumed by the background task or when the wait timeout expires.

My m68k assembly skills suck, so if you spot any error or cycle wasting code, you can tell me on twitter @doragasu, or open an issue here.

# Testing the ROM

Building the project should be straightforward if you have an `m68k-elf` toolchain available. Anyway if you cannot build it but want to test if the ROM works, you can download the automatically generated artifacts by browsing the [package registry in this repository](https://gitlab.com/doragasu/megadrive-tasking/-/packages). Make sure you use an accurate emulator or the real hardware, some emulators like Gens seem to have trouble implementing `trap` instructions.
