TARGET  = md_task
PREFIX ?= m68k-elf-
ifdef DEBUG
	CFLAGS  = -O0 -g -std=gnu99 -Wall -Wextra -m68000 -ffast-math -ffunction-sections
else
	CFLAGS  = -Os -std=gnu99 -Wall -Wextra -m68000 -fomit-frame-pointer -ffast-math -ffunction-sections -flto -ffat-lto-objects
endif
AFLAGS  = --register-prefix-optional -m68000
LFILE   = mdbasic.ld
LFLAGS  = -T $(LFILE) -Wl,-gc-sections
CC      = gcc
AS      = as
LD      = ld
GDB     = cgdb -d $(PREFIX)gdb --
OBJCOPY = objcopy
EMU ?= blastem
OBJDIR  = tmp

# List of directories with sources, excluding the boot stuff
DIRS=. gfx
OBJDIRS = $(foreach DIR, $(DIRS), $(OBJDIR)/$(DIR))
CSRCS = $(foreach DIR, $(DIRS), $(wildcard $(DIR)/*.c))
COBJECTS := $(patsubst %.c,$(OBJDIR)/%.o,$(CSRCS))
ASRCS = $(foreach DIR, $(DIRS), $(wildcard *.s))
AOBJECTS := $(patsubst %.s,$(OBJDIR)/%.o,$(ASRCS)) 

all: $(TARGET)

.PHONY: emu debug
emu: $(TARGET).bin
	rm -f $(HOME)/.local/share/blastem/$(TARGET)/save.nor
	@$(EMU) $< 2>/dev/null &

debug: $(TARGET).bin
	rm -f $(HOME)/.local/share/blastem/$(TARGET)/save.nor
	$(GDB) $(TARGET).elf -ex "target remote | blastem -D $(TARGET).bin"

$(TARGET): $(TARGET).bin
	dd if=$< of=$@ bs=8k conv=sync

$(TARGET).bin: $(TARGET).elf
	$(PREFIX)$(OBJCOPY) -O binary $< $@

$(TARGET).elf: boot/boot.o $(AOBJECTS) $(COBJECTS)
	$(PREFIX)$(CC) -o $(TARGET).elf boot/boot.o $(AOBJECTS) $(COBJECTS) $(LFLAGS) -Wl,-Map=$(OBJDIR)/$(TARGET).map -lgcc

boot/boot.o: boot/rom_head.bin boot/sega.s
	$(PREFIX)$(AS) $(AFLAGS) boot/sega.s -o boot/boot.o

boot/rom_head.bin: boot/rom_head.o
	$(PREFIX)$(OBJCOPY) -O binary $< $@

boot/rom_head.o: boot/rom_head.c
	$(PREFIX)$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: %.s | $(OBJDIRS)
	$(PREFIX)$(AS) $(AFLAGS) $< -o $@

$(OBJDIR)/%.o: %.c | $(OBJDIRS)
	$(PREFIX)$(CC) -c -MMD -MP $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: %.s | $(OBJDIRS)
	$(PREFIX)$(AS) $(AFLAGS) $< -o $@

$(OBJDIRS):
	mkdir -p $@

.PHONY: clean
clean:
	@rm -rf $(OBJDIR) boot/rom_head.bin boot/rom_head.o boot/boot.o $(TARGET).elf $(TARGET).bin

# Include auto-generated dependencies
-include $(patsubst %.c,$(OBJDIR)/%.d,$(CSRCS))

