#include <stdbool.h>

#include "tsk.h"
#include "vdp.h"
#include "util.h"

static volatile uint32_t count = 0;

/// Global initialization
static void init(void)
{
	// Initialize VDP
	VdpInit();
}

/// Background task running in user mode. Will take all the available CPU
/// not used by the foreground task
static void bg_tsk(void)
{
	uint16_t posted = false;
	while (true) {
		count++;
		if (!posted && count >= 0x100000) {
			posted = true;
			tsk_super_post(false);
		}
	}
}

static void pend_check(uint16_t timeout)
{
	const char *message;

	VdpDrawText(VDP_PLANEA_ADDR, 1, 3, VDP_TXT_COL_CYAN, 8, "PENDING",  ' ');
	if (tsk_super_pend(timeout)) {
		message = "TIMEOUT";
	} else {
		message = "UNLOCKED";
	}
	VdpDrawText(VDP_PLANEA_ADDR, 1, 3, VDP_TXT_COL_MAGENTA, 8, message,  ' ');

}

/// Foreground task running in privileged mode. Will be awakened once per frame
static void fg_tsk(void)
{
	while (true) {
		VdpDrawU32(VDP_PLANEA_ADDR, 1, 2, VDP_TXT_COL_CYAN, count);
		if (count > 0x10000 && count < 0x20000) {
			pend_check(180);
		} else if (count > 0x80000 && count < 0x90000) {
			pend_check(TSK_PEND_FOREVER);
		} else {
			tsk_user_yield();
		}
	}
}

__attribute__((interrupt)) static void vint_cb(void)
{
	static uint32_t frame_count = 0;

	VdpDrawU32(VDP_PLANEA_ADDR, 1, 1, VDP_TXT_COL_WHITE, frame_count++);
}

/// Entry point
int entry_point(uint16_t hard)
{
	UNUSED_PARAM(hard);

	init();

	// Configure vertical blanking interrupt callback
	vint_cb_set(vint_cb);
	// Configure background task as user task
	tsk_user_set(bg_tsk);

	// Start foreground task
	fg_tsk();

	// We should never reach here!
	return 1;
}
